//
//  ShowCellHeaderTemplate.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 23/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

class ShowCellHeaderTemplate: UICollectionReusableView {
    @IBOutlet weak var myShowsHeaderLabel: UILabel!
    
    func setHeader(text: String) {
        myShowsHeaderLabel.text = text
    }
}

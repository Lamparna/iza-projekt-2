//
//  FindShowsViewController.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 21/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import UIKit
import Kingfisher
import SkeletonView
import NotificationBannerSwift

class FindShowsViewController: UIViewController, SkeletonTableViewDelegate, SkeletonTableViewDataSource {
    var dataSource = [TVMazeShowSearch]()
    
    @IBOutlet weak var findShowInput: UITextField!
    @IBOutlet weak var findShowButton: UIButton!
    @IBOutlet weak var findShowTableView: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "showCell") as! SearchCellTemplate
        let searchResult = dataSource[indexPath.row]
        
        cell.setShow(show: searchResult.show)
        
        return cell
    }
    
    func getSearchValue() -> String?
    {
        guard let value = findShowInput.text else { return nil }
                
        let resultString = value.replacingOccurrences(of: #"[\W]"#, with: "-",options: .regularExpression)
        
        return resultString
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "showCell"
    }
    
    @IBAction func findShowButtonTouchEnd(_ sender: Any) {
        view.showAnimatedSkeleton()
        
        guard let searchValue = getSearchValue() else { return }
        
        let url = URL(string: "https://api.tvmaze.com/search/shows?q=\(searchValue)")
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if let _data = data {
                do {
                    if let stringData = String(data: _data, encoding: .utf8) {
                        let jsonResponse = try JSONDecoder().decode([TVMazeShowSearch].self, from: stringData.data(using: .utf8)!)
                        
                        self.dataSource = jsonResponse
                        
                        DispatchQueue.main.async {
                            self.view.hideSkeleton()
                            self.findShowTableView.reloadData()
                        }
                    }
                }
                catch
                {
                    Helpers.showErrorNotification(text: error.localizedDescription)
                }
            }
            else if let _error = error {
                Helpers.showErrorNotification(text: _error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showFoundShowDetail") {
            if(segue.destination.isKind(of: ShowDetailViewController.self)) {
                let detailVc = segue.destination as! ShowDetailViewController
                let cell = sender as! SearchCellTemplate
                detailVc.selectedShowId = cell.showId
                detailVc.displaySource = DetailSource.Search
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
                
        findShowTableView.separatorColor = UIColor.clear
    }
}

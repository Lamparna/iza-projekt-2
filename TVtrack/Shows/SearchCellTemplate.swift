//
//  SearchCellTemplate.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 21/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import SkeletonView

class SearchCellTemplate: UITableViewCell {
    
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showNameLabel: UILabel!
    @IBOutlet weak var showDescLabel: UILabel!
    @IBOutlet weak var showGenresLabel: UILabel!
    
    var showId: Int!
    
    func setShow(show: TVMazeShow)
    {
        showId = show.id
        
        showNameLabel.text = show.name
        
       
        var networkString = "";
        
        if(show.network != nil) {
            networkString = show.network!.name
        }
        else {
            networkString = show.webChannel!.name
        }
        
        if let _premiereDate = show.premiered {
            let premiered = _premiereDate[0..<4]
            showDescLabel.text = "\(premiered) • \(networkString)"
        }
        
        showGenresLabel.text = ""
        for genre in show.genres {
            showGenresLabel.text = "\(showGenresLabel.text!)\(genre) "
        }
                
        if let _image = show.image {
            let httpsUrl = _image.medium.replace(target: "http", withString: "https")
            
            let imgUrl = URL(string: httpsUrl)
            
            self.showImage.kf.setImage(with: imgUrl)
        }
    }
}

//
//  ShowCellTemplate.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 23/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import SkeletonView

class ShowCellTemplate : UICollectionViewCell {
    @IBOutlet weak var showImageView: UIImageView!
    @IBOutlet weak var showNameLabel: UILabel!
    
    var show = Show()
    
    func setShow(show: Show) {
        self.show = show
        
        let httpsUrl =  show.imgurl.replace(target: "http", withString: "https")

        let imgUrl = URL(string: httpsUrl)
        
        self.showImageView.kf.setImage(with: imgUrl)
        self.showNameLabel.text = show.name
    }
}

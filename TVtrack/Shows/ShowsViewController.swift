//
//  ShowsViewController.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 23/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SkeletonView

class ShowsViewController: UICollectionViewController, SkeletonCollectionViewDataSource, SkeletonCollectionViewDelegate {
    @IBOutlet var showsCollectionView: UICollectionView!
    
    var dataSource = [Show]()
    var realm: Realm!
    
    override func viewDidLoad() {
        realm = try! Realm()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.showAnimatedSkeleton()
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
        }
        
        let shows = realm.objects(Show.self)
        
        view.hideSkeleton()
        dataSource = Array(shows).reversed()
        
        self.showsCollectionView.reloadData()
    }
    
    func collectionSkeletonView(_ skeletonView: UICollectionView, cellIdentifierForItemAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "showCell"
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "showCell", for: indexPath) as! ShowCellTemplate
                
        if indexPath.row < dataSource.count {
            cell.setShow(show: dataSource[indexPath.row])
        }
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "showHeaderCell", for: indexPath) as! ShowCellHeaderTemplate
        
        view.setHeader(text: "Recently Added Shows")
        
        return view
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "showShowDetail") {
            if (segue.destination.isKind(of: ShowDetailViewController.self)) {
                let detailVc = segue.destination as! ShowDetailViewController
                let cell = sender as! ShowCellTemplate
                detailVc.selectedShowId = cell.show.id
                detailVc.displaySource = DetailSource.Home
            }
        }
    }
}

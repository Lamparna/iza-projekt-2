//
//  SeasonCellTemplate.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 21/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SkeletonView
import SnapKit

class SeasonCellTemplate: UITableViewCell
{
    @IBOutlet weak var seasonNameLabel: UILabel!
    @IBOutlet weak var seasonButton: UIButton!
    
    var apiSeason: TVMazeShowSeason!
    var apiEpisodes = [TVMazeShowEpisode]()
    var episodeIds = [Int]()
    var apiShow: TVMazeShow!
    
    var realm: Realm!
    
    func disableButton() {
        self.seasonButton.isHidden = true
        self.seasonButton.snp.updateConstraints { (main) in
            main.width.equalTo(0)
        }
    }
    
    func setSeason(apiSeason: TVMazeShowSeason, apiEpisodes: [TVMazeShowEpisode], show: TVMazeShow) {
        var seasonName = apiSeason.name
        
        if seasonName == "" {
            seasonName = "Season \(apiSeason.number ?? 0)"
        }
        
        self.seasonNameLabel.text = seasonName
        self.apiSeason = apiSeason
        self.apiEpisodes = apiEpisodes
        self.apiShow = show
        
        episodeIds = [Int]()
        for episode in apiEpisodes {
            if episode.season == apiSeason.number {
                episodeIds.append(episode.id)
            }
        }
        
        realm = try! Realm()
        
        let episodesResult = realm.objects(Episode.self).filter("id in %@", episodeIds)
            
        if episodesResult.count == episodeIds.count {
            self.seasonButton.isHidden = true
        }
        else {
            self.seasonButton.isHidden = false
        }
        
        self.seasonButton.snp.makeConstraints{ (main) in
            main.right.equalToSuperview().offset(-17)
            main.width.equalTo(30)
            main.top.equalToSuperview().offset(5)
        }
        
        self.seasonNameLabel.snp.makeConstraints { (main) in
            main.left.equalToSuperview().offset(20)
            main.right.equalTo(self.seasonButton.snp.left).offset(-20)
            main.top.equalToSuperview().offset(10)
        }
    }
    
    @IBAction func seasonButtonTouchUp(_ sender: Any) {
        realm = try! Realm()
        
        let episodesResult = realm.objects(Episode.self).filter("id in %@", episodeIds)
        
        var episodesToAdd = [TVMazeShowEpisode]()
        for apiEpisode in apiEpisodes {
            if !episodesResult.contains(where: { (dbEpisode) in
                dbEpisode.id == apiEpisode.id
            }) {
                episodesToAdd.append(apiEpisode)
            }
        }
        
        let showResult = realm.objects(Show.self).filter("id == \(apiShow.id)")
        
        var show: Show!
        if showResult.count > 0 {
            show = showResult.first!
        }
        else {
            show = Show()
            show.id = apiShow.id
            show.name = apiShow.name
            show.imgurl = apiShow.image!.medium
            show.addedDate = Date()
            
            try! realm.write {
                realm.add(show)
                Helpers.showInfoNotification(text: "Show Added")
            }
        }
        
        for episodeToAdd in episodesToAdd {
            let episode = Episode()
            episode.id = episodeToAdd.id
            episode.name = episodeToAdd.name
            episode.imgurl = episodeToAdd.image?.medium
            episode.addedDate = Date()
            episode.show = show
            
            try! realm.write {
                realm.add(episode)
            }
        }
        
        disableButton()
    }
}
